# Iproute2 - conan recipe

[Conan.io][conan_site] recipe to build [iproute2 utilities][homepage] library.

[conan_site]: https://www.conan.io/
[homepage]: https://wiki.linuxfoundation.org/networking/iproute2


## About iproute2

> Iproute2 is a collection of utilities for controlling TCP / IP
  networking and traffic control in Linux.

> iproute2 is usually shipped in a package called iproute or iproute2 and consists
  of several tools, of which the most important are ip and tc. ip controls IPv4 and
  IPv6 configuration and tc stands for traffic control.

## How to use

See [conan docs][conan_docs] for instructions in how to use conan.

Documentation about iproute2 are available [here][program_docs].

[conan_docs]: http://docs.conan.io/en/latest/
[program_docs]: http://www.policyrouting.org/iproute2.doc.html


## License

This conan recipe is distributed under the [unlicense](http://unlicense.org/) terms
(see UNLICENSE.md).

Iproute2 is distributed under the GPLv2 license (see the COPYING file, included
with the built package and source code).
