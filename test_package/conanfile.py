#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile, tools

class TestRecipe(ConanFile):

    def test(self):
        if tools.cross_building(self.settings):
            self.output.info("Cross building - skip run test")
            return

        self.output.info("running built ip command:")

        cmd = "ip -V"

        self.output.info(cmd)
        self.run(cmd)
