# Get distro version
cat /etc/lsb-release
. /etc/lsb-release

echo "DISTRIB_CODENAME = $DISTRIB_CODENAME"

# Create file with repository where bison is
echo "deb http://archive.ubuntu.com/ubuntu/ $DISTRIB_CODENAME main restricted" > source.list

# Update only that repository
sudo apt-get update -o Dir::Etc::sourcelist="./source.list" \
    -o Dir::Etc::sourceparts="-" -o APT::Get::List-Cleanup="0"

# Install bison
sudo apt-get install -y bison flex
